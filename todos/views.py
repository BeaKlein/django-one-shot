from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem

def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos
    }
    return render(request, "todos/todo_list_list.html", context)

def todo_list_detail(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "details": details
    }
    return render(request, 'todos/todo_list_detail.html', context)
